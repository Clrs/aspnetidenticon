﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            font-family: "Helvetica Neue" , Helvetica,Arial, 'Microsoft YaHei' , sans-serif;
            font-size: 14px; 
            margin: 20px 0 0 50px;
        }
        .media, .media-body 
        {
            overflow: hidden;
            zoom: 1;
        }
        .media > .pull-left 
        {
            margin-right: 10px;
        }
        .media-heading 
        {
            margin: 0 0 5px 10px;
        }
        .list-unstyled 
        {
            list-style: none;
            padding-left: 10px;
            margin-top: 0px;
        }
        img.avatar 
        {
            background-color: #fff;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            -moz-box-shadow: 1px 1px 2px #aaa;
            -webkit-box-shadow: 1px 1px 2px #aaa;
            box-shadow: 1px 1px 2px #aaa;
            padding: 1px;
        }
        .media-object 
        {
            display: block;
        }
        .highlight 
        {
            padding: 9px 14px;
            margin-bottom: 14px;
            background-color: #fff;
            border: 1px solid #e1e1e8;
            border-radius: 4px;
            padding-bottom: 0px;
            width: 20%;
        }
        .text-muted
        {
            color: #999999;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h4>generate default identicon (c# version)</h4>
        <div class="media text-muted highlight">
            <a href="javascript:void(0);" style="float: left;">
                <img class="media-object avatar" src="user_001.png" alt="用户头像" style="width: 50px; height: 50px;" />
            </a>
            <div class="media-body">
                <h4 class="media-heading">
                    <a href="javascript:void(0);" style="color: #428bca;">pinopino</a></h4>
                <div>
                    <ul class="list-unstyled">
                        <li><small>伪宅以上腐女未满，不明真相的围观群众。</small></li>
                        <li><small>2014-3-5 10:22:03</small></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
