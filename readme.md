﻿AspnetIdenticon
====================

## 这是什么
- - -

你能在这里看到很漂亮的系统自动生成[头像](https://github.com/blog/1586-identicons)，人们把这东西称作[identicons](http://en.wikipedia.org/wiki/Identicon)。
目前能找到的版本大多数是php、ruby或者python的，于是谋生了翻译一个c#版本的想法。

## 如何使用
- - -

非常简单，你所需要的就是clone这个项目，使用vs加载这个项目，f5运行它，搞定！

首先来生成一张图片，像这样访问如下地址：

	http://your-local-address/identiconhandler/handler.ashx?data=your-data-here

没有任何问题的话，现在站点的根目录下已经生成好了一张名为“user_001.png”的图片，接下来访问：

	http://your-local-address/identiconhandler/default.aspx

没有任何问题的话，你应该看到类似下面这样的：

![](http://images.cnitblog.com/i/271595/201403/041243531933826.png)

：）

## 问题反馈
- - -

有任何疑问或问题，欢迎通过issue或者我的[博客](http://www.cnblogs.com/pinopino/)交流：）